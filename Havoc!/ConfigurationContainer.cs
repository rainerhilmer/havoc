﻿using System.Collections.Generic;
using Cyron43.GtaIV.Common;

namespace Havoc
{
   public class ConfigurationContainer : IConfigurationContainer
   {
      public bool CarClearers { get; set; }
      public bool CarClearersDriveToWaypoint { get; set; }
      public bool CarClearersObeyTrafficLaws { get; set; }
      public float CarClearersPerimeter { get; set; }
      public float CarClearersSpeed { get; set; }
      public bool CreateCarClearersOutside { get; set; }
      public bool InvolvePlayer { get; set; }
      public bool KeepMadDrivers { get; set; }
      public List<KeyContainer> Keys { get; set; }
      public bool MadCops { get; set; }
      public bool MadDrivers { get; set; }
      public bool MadDriversObeyTrafficLaws { get; set; }
      public float MadDriversSpeed { get; set; }
      public bool MadPeds { get; set; }
      public int Version { get; set; }
   }
}