﻿using System;
using System.Linq;
using Cyron43.GtaIV.Common;
using GTA;

namespace Havoc
{
   // ReSharper disable once ClassNeverInstantiated.Global
   public class Core : Script
   {
      /// <summary>
      ///    It seems the .net scripthook is not thread safe. So there must be a
      ///    lock to avoid injection of alien data and to avoid disposal of own
      ///    objects through other mods.
      /// </summary>
      private static readonly object SyncLock = new object();

      private readonly KeyHandling _keyHandling;
      private ErrorType _typeOfError;

      public Core()
      {
         ReadConfiguration();
         if(_typeOfError != ErrorType.None)
            return;
         _keyHandling = new KeyHandling(this);
         MadDriversHandling = new MadDriversHandling();
         KeyDown += OnKeyDown;
         Interval = 250;
         Tick += OnTick;
      }

      internal ConfigurationContainer Configuration { get; private set; }
      internal MadDriversHandling MadDriversHandling { get; private set; }

      internal void ReadConfiguration()
      {
         Configuration = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(ModIdentityProvider.Identity, out _typeOfError);
      }

      internal void StartHavoc()
      {
         lock(SyncLock)
         {
            CommonFunctions.DisplayText("Causing havoc!", 3000);
            var peds = CommonFunctions.GetPedsSafe(Player.Character.Position, 50.0f);
            if(peds.Length == 0)
               return;
            foreach(var ped in peds.Where(CommonFunctions.PedExists))
            {
               ped.CanSwitchWeapons = true;
               GoMadCop(ped);
               if(!ped.isInVehicle())
               {
                  GoMadPed(ped);
               }
               else
               {
                  MadDriversHandling.GoMadDriver(this, ped);
               }
            }
         }
      }

      private void GoMadCop(Ped cop)
      {
         lock(SyncLock)
         {
            if(!CommonFunctions.PedExists(cop) || ReferenceEquals(cop, Player.Character))
               return;
            if(
               Configuration.MadCops
               && cop.PedType == PedType.Cop
               && !cop.isRequiredForMission)
               cop.StartKillingSpree(Configuration.InvolvePlayer);
         }
      }

      private void GoMadPed(Ped ped)
      {
         lock(SyncLock)
         {
            if(!Configuration.MadPeds)
               return;
            if(!CommonFunctions.PedExists(ped) || ReferenceEquals(ped, Player.Character))
               return;
            if(!CommonFunctions.PedExists(ped) || ped.PedType == PedType.Cop)
               return;
            if(CommonFunctions.PedExists(ped) && !ped.isRequiredForMission)
               ped.StartKillingSpree(Configuration.InvolvePlayer);
         }
      }

      private void OnKeyDown(object sender, KeyEventArgs e)
      {
         ReadConfiguration();
         if(_typeOfError != ErrorType.None)
            return;
         _keyHandling.HandleKey(e);
      }

      private void OnTick(object sender, EventArgs e)
      {
         MadDriversHandling.HandleMadDrivers();
      }
   }
}