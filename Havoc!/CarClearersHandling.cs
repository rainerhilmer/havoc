﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cyron43.GtaIV.Common;
using GTA;

namespace Havoc
{
   public class CarClearersHandling : Script
   {
      private static readonly object SyncLock = new object();
      private readonly Core _core;
      private Vector3 _currentWaypoint;
      private List<Vehicle> _vehiclesToClear;
      // ReSharper disable once UnusedMember.Global
      public CarClearersHandling()
      {
      }

      public CarClearersHandling(Core core)
      {
         _core = core;
         Interval = 250;
         core.Tick += HandleCarClearers;
         _vehiclesToClear = new List<Vehicle>();
         var waypointBlip = Game.GetWaypoint();
         _currentWaypoint = waypointBlip != null ? Game.GetWaypoint().Position : new Vector3();
      }

      /// <summary>
      ///    This method is called by the key handling.
      /// </summary>
      internal void TriggerCarClearers()
      {
         CommonFunctions.DisplayText("Car clearers called.", 3000);
         _vehiclesToClear.Clear();
         var waypointBlip = Game.GetWaypoint();
         _currentWaypoint = waypointBlip != null ? waypointBlip.Position : new Vector3();
         _vehiclesToClear = GetAbandonedVehiclesAround();
         GenerateCarClearers();
      }

      private void CarClearerDrive(ref Ped carClearer, Vehicle vehicle)
      {
         vehicle.CloseAllDoors();
         if(_core.Configuration.CarClearersDriveToWaypoint && _currentWaypoint != default(Vector3))
         {
            carClearer.Task.AlwaysKeepTask = true;
            carClearer.Task.DriveTo(
               _currentWaypoint,
               CommonFunctions.CorrectedSpeed(_core.Configuration.CarClearersSpeed),
               _core.Configuration.CarClearersObeyTrafficLaws,
               AllowToDriveRoadsWrongWay: false);
         }
         else
         {
            if(CommonObjects.EmergencyVehicles.Any(model => vehicle.Model == model))
               OverrideDrivingBehaviorForEmergencyVehicles(ref carClearer);
            else
               carClearer.Task.CruiseWithVehicle(
                  vehicle,
                  CommonFunctions.CorrectedSpeed(_core.Configuration.CarClearersSpeed),
                  _core.Configuration.CarClearersObeyTrafficLaws);
            carClearer.NoLongerNeeded();
            vehicle.NoLongerNeeded();
         }
      }

      private void CarClearerStopVehicleAndLeave(Vehicle vehicle)
      {
         lock(SyncLock)
         {
            var carClearer = vehicle.GetPedOnSeat(VehicleSeat.Driver);
            if(!CommonFunctions.PedExists(carClearer))
               return;
            carClearer.Task.CruiseWithVehicle(
               vehicle, 0.0f,
               ObeyTrafficLaws: _core.Configuration.CarClearersObeyTrafficLaws);
            WaitForVehicleToStop(vehicle);
            carClearer.Task.AlwaysKeepTask = false;
            carClearer.BlockPermanentEvents = false;
            carClearer.ChangeRelationship(RelationshipGroup.Player, Relationship.Neutral);
            carClearer.Task.ClearAll();
            carClearer.LeaveGroup();
            carClearer.LeaveVehicle();
            Wait(3000);
            carClearer.FreezePosition = false; // peds get stuck sometimes but that's a game bug.
            carClearer.Task.WanderAround();
            carClearer.NoLongerNeeded();
            carClearer.isRequiredForMission = false;
            if(_currentWaypoint != default(Vector3)
               && Player.Character.Position.DistanceTo(_currentWaypoint) > 200.0f)
               carClearer.Delete();
            Game.LoadAllPathNodes = false;
            if(Game.Exists(vehicle))
            {
               vehicle.FreezePosition = false;
               vehicle.NoLongerNeeded();
            }
            _vehiclesToClear.Remove(vehicle);
         }
      }

      private void GenerateCarClearers()
      {
         foreach(var vehicle in _vehiclesToClear.Where(CommonFunctions.VehicleExists))
         {
            vehicle.DoorLock = DoorLock.None;
            Ped carClearer;
            if(_core.Configuration.CreateCarClearersOutside)
            {
               carClearer = World.CreatePed(vehicle.Position.Around(10.0f).ToGround());
               carClearer.Task.AlwaysKeepTask = true;
               carClearer.Task.EnterVehicle(vehicle, VehicleSeat.Driver);
            }
            else
               carClearer = vehicle.CreatePedOnSeat(VehicleSeat.Driver);
            if(!CommonFunctions.PedExists(carClearer))
               continue;
            CarClearerDrive(ref carClearer, vehicle);
         }
         //_vehiclesToClear.Clear();
      }

      private List<Vehicle> GetAbandonedVehiclesAround()
      {
         var vehiclesRawList = CommonFunctions.GetVehiclesSafe(Player.Character.Position,
            _core.Configuration.CarClearersPerimeter)
            .Where(vehicle =>
               !vehicle.isOnFire
               && vehicle.isAlive
               && !ReferenceEquals(vehicle, Player.Character.CurrentVehicle))
            .ToArray();
         var vehiclesCleanList = new List<Vehicle>();
         if(vehiclesRawList.Length == 0)
            return vehiclesCleanList;
         foreach(var vehicle in vehiclesRawList)
         {
            vehicle.Repair();
            vehicle.Wash();
            if(!vehicle.isSeatFree(VehicleSeat.Driver) || !vehicle.isDriveable)
               continue;
            if(!_core.Configuration.InvolvePlayer
               && ReferenceEquals(vehicle, Player.LastVehicle))
               continue;
            // Wash and repair any heli but don't fly it.
            if(vehicle.Model.isHelicopter)
               continue;
            vehiclesCleanList.Add(vehicle);
         }
         return vehiclesCleanList;
      }

      private void HandleCarClearers(object sender, EventArgs e)
      {
         _core.ReadConfiguration();
         ReleaseCarClearersWhenWaypointIsReached();
         ReleaseCarClearersWhenOffScreen();
      }

      private void OverrideDrivingBehaviorForEmergencyVehicles(ref Ped ped)
      {
         Wait(500);
         if(ped.CurrentVehicle.SirenActive)
            ped.Task.CruiseWithVehicle(
               ped.CurrentVehicle,
               CommonFunctions.CorrectedSpeed(65.0f),
               ObeyTrafficLaws: false);
      }

      private void ReleaseCarClearersWhenOffScreen()
      {
         if(_core.Configuration.CarClearers
            && !_core.Configuration.CarClearersDriveToWaypoint
            || _currentWaypoint == default(Vector3)
            && _vehiclesToClear != null
            && _vehiclesToClear.Count > 0)
         {
            var tempList = _vehiclesToClear.ToList();
            foreach(var vehicle in tempList)
            {
               var carClearer = vehicle.GetPedOnSeat(VehicleSeat.Driver);
               if(!CommonFunctions.VehicleExists(vehicle))
                  return;
               CommonFunctions.DeleteWhenOffScreen(
                  vehicle, Player.Character.Position, carClearer, 50f);
               _vehiclesToClear.Remove(vehicle);
            }
         }
      }

      private void ReleaseCarClearersWhenWaypointIsReached()
      {
         if(!_core.Configuration.CarClearersDriveToWaypoint
            || !_core.Configuration.CarClearers
            || _vehiclesToClear == null
            || _vehiclesToClear.Count <= 0)
            return;
         var tempVehicleList = _vehiclesToClear.ToList();
         foreach(var vehicle in tempVehicleList)
         {
            if(_currentWaypoint == default(Vector3))
            {
               ReleaseCarClearersWhenOffScreen();
               continue;
            }
            if(Game.Exists(vehicle) && vehicle.Position.DistanceTo2D(_currentWaypoint) < 10.0f)
               CarClearerStopVehicleAndLeave(vehicle);
         }
      }

      private void WaitForVehicleToStop(Vehicle currentVehicle)
      {
         while(currentVehicle.Speed > 1)
            Wait(Interval);
      }
   }
}