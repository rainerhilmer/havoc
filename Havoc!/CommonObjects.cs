﻿using GTA;

namespace Havoc
{
   internal static class CommonObjects
   {
      internal static readonly Model[] EmergencyVehicles =
      {
         "AMBULANCE",
         "FBI",
         "FIRETRUK",
         "NOOSE",
         "NSTOCKADE",
         "POLICE",
         "POLICE2",
         "POLICE3",
         "POLICE4",
         "POLICEB",
         "POLPATRIOT",
         "PSTOCKADE"
      };
   }
}