﻿using GTA;

namespace Havoc
{
   internal class MadDriversData
   {
      internal bool DriverIsDead { get; set; }
      internal bool DriverIsInjured { get; set; }
      internal bool DriverLeftVehicle { get; set; }
      internal Ped MadDriver { get; set; }
      internal Vehicle MadDriversVehicle { get; set; }
      internal bool VehicleIsDestroyed { get; set; }
      internal string VehicleName { get; set; }
   }
}