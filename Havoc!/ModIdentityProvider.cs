﻿using System.Reflection;
using Cyron43.GtaIV.Common;

namespace Havoc
{
   internal static class ModIdentityProvider
   {
      internal static IModIdentity Identity
      {
         get
         {
            return new ModIdentity
                   {
                      AssemblyName = Assembly.GetExecutingAssembly().GetName().Name,
                      FullPath = CommonFunctions.FileRepositoryPath + "HavocConfig.xml",
                      Version = Assembly.GetExecutingAssembly().GetName().Version.Major
                   };
         }
      }
   }
}