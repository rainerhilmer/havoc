﻿using Cyron43.GtaIV.Common;
using GTA;

namespace Havoc
{
   public class KeyHandling : Script
   {
      private readonly CarClearersHandling _carClearersHandling;
      private readonly Core _core;
      // ReSharper disable once UnusedMember.Global
      public KeyHandling()
      {
      }

      public KeyHandling(Core core)
      {
         _core = core;
         _carClearersHandling = new CarClearersHandling(core);
      }

      internal void HandleKey(KeyEventArgs e)
      {
         AtStartHavoc(e);
         AtReleaseMadDrivers(e);
         AtToggleBlipMap(e);
         AtTriggerCarClearers(e);
      }

      private void AtReleaseMadDrivers(KeyEventArgs e)
      {
         if(
            !KeyHandlingCommons.KeyIs(
               KeyHandlingCommons.GetKeyContainer(
                  _core.Configuration.Keys, "ReleaseMadDrivers"), e))
            return;
         _core.MadDriversHandling.ReleaseMadDriversAndTheirVehicles();
      }

      private void AtStartHavoc(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "StartHavoc"), e))
            return;
         _core.StartHavoc();
      }

      private void AtToggleBlipMap(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "ToggleBlipMap"), e))
            return;
         _core.MadDriversHandling.TogglePersistentMadDriversMap();
      }

      private void AtTriggerCarClearers(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "TriggerCarClearers"), e))
            return;
         _carClearersHandling.TriggerCarClearers();
      }
   }
}