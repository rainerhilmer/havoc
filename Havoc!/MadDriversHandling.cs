﻿using System.Collections.Generic;
using System.Linq;
using Cyron43.GtaIV.Common;
using GTA;

namespace Havoc
{
   public class MadDriversHandling : Script
   {
      private const uint MAX_AMOUNT_OF_ITEMS = 20;
      private static readonly object SyncLock = new object();
      private readonly List<Blip> _blips;
      internal readonly List<MadDriversData> MadDriversAndTheirVehicles;

      public MadDriversHandling()
      {
         MadDriversAndTheirVehicles = new List<MadDriversData>();
         _blips = new List<Blip>();
      }

      internal bool BlipMapActive { get; private set; }

      internal void AddMadDriverAndHisVehicle(Ped madDriver, Vehicle hisVehicle)
      {
         if(MadDriversAndTheirVehicles.Count > 0)
            ClearNonExistingItems();
         if(!CommonFunctions.VehicleExists(hisVehicle))
            return;
         if(!madDriver.isInVehicle(hisVehicle))
            return;
         if(MadDriverIsAlreadyInList(madDriver))
            return;
         madDriver.isRequiredForMission = false;
         hisVehicle.isRequiredForMission = false;
         var item = new MadDriversData
                    {
                       MadDriver = madDriver,
                       MadDriversVehicle = hisVehicle,
                       VehicleName = hisVehicle.Name
                    };
         MadDriversAndTheirVehicles.Add(item);
         RestrictAmountOfItems();
      }

      internal void ClearNonExistingItems()
      {
         var tempList = CreateTempList();
         foreach(var item in tempList)
         {
            if(CommonFunctions.PedExists(item.MadDriver) && CommonFunctions.VehicleExists(item.MadDriversVehicle))
               continue;
            MarkItemForNoLongerNeeded(item);
            MadDriversAndTheirVehicles.Remove(item);
         }
      }

      internal void GoMadDriver(Core core, Ped ped)
      {
         lock(SyncLock)
         {
            if(!core.Configuration.MadDrivers
               || !CommonFunctions.PedExists(ped)
               || ReferenceEquals(ped, Player.Character)
               || ped.isRequiredForMission)
               return;
            if(!CommonFunctions.PedExists(ped) || !CommonFunctions.VehicleExists(ped.CurrentVehicle))
               return;
            if(CommonObjects.EmergencyVehicles.Any(model => ped.CurrentVehicle.Model == model)
               && ped.CurrentVehicle.SirenActive)
               OverrideDrivingBehaviorForEmergencyVehicles(ref ped);
            else
               ped.Task.CruiseWithVehicle(
                  ped.CurrentVehicle,
                  CommonFunctions.CorrectedSpeed(core.Configuration.MadDriversSpeed),
                  core.Configuration.MadDriversObeyTrafficLaws);
            HandleMadDriversPersistance(ref core, ped);
         }
      }

      internal void HandleMadDrivers()
      {
         if(MadDriversAndTheirVehicles.Count > 0)
         {
            ClearNonExistingItems();
            ReleaseOnDeadMadDrivers();
            ReleaseOnInjuredMadDrivers();
            ReleaseOnDestroyedVehicles();
            ReleaseOnDetachedMadDriver();
            if(BlipMapActive)
            {
               UpdatePersistentMadDriversMap();
               Game.DisplayText("Persistant mad drivers count: "
                                + MadDriversAndTheirVehicles.Count, Interval + 500);
            }
         }
      }

      internal void ReleaseMadDriversAndTheirVehicles()
      {
         if(MadDriversAndTheirVehicles.Count == 0)
            return;
         CommonFunctions.DisplayText("Releasing persistent mad drivers.", 3000);
         ClearNonExistingItems();
         foreach(var item in MadDriversAndTheirVehicles)
         {
            MarkItemForNoLongerNeeded(item);
         }
      }

      internal void ReleaseOnDeadMadDrivers()
      {
         var tempList = CreateTempList();
         foreach(var item in tempList.Where(item => CommonFunctions.PedExists(item.MadDriver) && item.MadDriver.isDead))
         {
            MarkItemForNoLongerNeeded(item);
            MadDriversAndTheirVehicles.Remove(item);
            MadDriversInformation(new MadDriversData
                                  {
                                     DriverIsDead = true,
                                     VehicleName = item.VehicleName
                                  });
         }
      }

      internal void ReleaseOnDestroyedVehicles()
      {
         var tempList = CreateTempList();
         foreach(var item in tempList
            .Where(item => CommonFunctions.VehicleExists(item.MadDriversVehicle)
                           && !item.MadDriversVehicle.isAlive))
         {
            MarkItemForNoLongerNeeded(item);
            MadDriversAndTheirVehicles.Remove(item);
            MadDriversInformation(new MadDriversData
                                  {
                                     VehicleIsDestroyed = true,
                                     VehicleName = item.VehicleName
                                  });
         }
      }

      internal void ReleaseOnDetachedMadDriver()
      {
         var tempList = CreateTempList();
         foreach(var item in tempList
            .Where(item => CommonFunctions.PedExists(item.MadDriver)
                           && CommonFunctions.VehicleExists(item.MadDriversVehicle)
                           && !item.MadDriver.isInVehicle(item.MadDriversVehicle)))
         {
            MarkItemForNoLongerNeeded(item);
            MadDriversAndTheirVehicles.Remove(item);
            MadDriversInformation(new MadDriversData
                                  {
                                     DriverLeftVehicle = true,
                                     VehicleName = item.VehicleName
                                  });
         }
      }

      internal void ReleaseOnInjuredMadDrivers()
      {
         var tempList = CreateTempList();
         foreach(var item in tempList
            .Where(item => CommonFunctions.PedExists(item.MadDriver) && item.MadDriver.isInjured))
         {
            MarkItemForNoLongerNeeded(item);
            MadDriversAndTheirVehicles.Remove(item);
            MadDriversInformation(
               new MadDriversData
               {
                  DriverIsInjured = true,
                  VehicleName = item.VehicleName
               });
         }
      }

      internal void TogglePersistentMadDriversMap()
      {
         lock(SyncLock)
         {
            if(!BlipMapActive)
            {
               if(!MadDriversOnRampage())
                  return;
               CreateBlipMap();
               BlipMapActive = true;
            }
            else
            {
               ClearBlipMap();
               BlipMapActive = false;
            }
         }
      }

      internal void UpdatePersistentMadDriversMap()
      {
         ClearBlipMap();
         CreateBlipMap();
      }

      private static void MarkItemForNoLongerNeeded(MadDriversData item)
      {
         if(CommonFunctions.PedExists(item.MadDriver))
         {
            item.MadDriver.isRequiredForMission = false;
            item.MadDriver.NoLongerNeeded();
         }
         if(!CommonFunctions.VehicleExists(item.MadDriversVehicle))
            return;
         item.MadDriversVehicle.isRequiredForMission = false;
         item.MadDriversVehicle.NoLongerNeeded();
      }

      private static void OverrideDrivingBehaviorForEmergencyVehicles(ref Ped ped)
      {
         ped.Task.CruiseWithVehicle(
            ped.CurrentVehicle,
            CommonFunctions.CorrectedSpeed(65.0f),
            ObeyTrafficLaws: false);
         ped.CurrentVehicle.SirenActive = true;
      }

      private void ClearBlipMap()
      {
         foreach(var blip in _blips)
         {
            blip.Display = BlipDisplay.Hidden;
            blip.Delete();
         }
         _blips.Clear();
      }

      private void CreateBlipMap()
      {
         foreach(var item in MadDriversAndTheirVehicles)
         {
            var blip = Blip.AddBlip(new Vector3(item.MadDriversVehicle.Position.X,
               item.MadDriversVehicle.Position.Y, item.MadDriversVehicle.Position.Z));
            blip.Icon = BlipIcon.Misc_Destination;
            blip.RouteActive = false;
            blip.Display = BlipDisplay.MapOnly;
            blip.Color = BlipColor.Purple;
            blip.Name = item.VehicleName;
            _blips.Add(blip);
         }
      }

      private IEnumerable<MadDriversData> CreateTempList()
      {
         return MadDriversAndTheirVehicles.ToList();
      }

      private void HandleMadDriversPersistance(ref Core core, Ped ped)
      {
         if(core.Configuration.KeepMadDrivers)
            AddMadDriverAndHisVehicle(ped, ped.CurrentVehicle);
         else
            ReleaseMadDriversAndTheirVehicles();
      }

      private bool MadDriverIsAlreadyInList(Ped madDriver)
      {
         return MadDriversAndTheirVehicles.Exists(p => p.MadDriver == madDriver);
      }

      private void MadDriversInformation(MadDriversData information)
      {
         var remaining = " " + MadDriversAndTheirVehicles.Count + " mad drivers remaining.";
         if(information.DriverIsDead && information.VehicleIsDestroyed)
         {
            CommonFunctions.DisplayText("Driver of "
                                        + information.VehicleName
                                        + " died and vehicle is destroyed."
                                        + remaining, 3000);
            return;
         }
         if(information.DriverIsDead)
         {
            CommonFunctions.DisplayText("Driver of "
                                        + information.VehicleName
                                        + " died."
                                        + remaining, 3000);
            return;
         }
         if(information.VehicleIsDestroyed)
         {
            CommonFunctions.DisplayText(information.VehicleName
                                        + " was destroyed."
                                        + remaining, 3000);
            return;
         }
         if(information.DriverIsInjured)
         {
            CommonFunctions.DisplayText("Driver of "
                                        + information.VehicleName
                                        + " got injured and was released."
                                        + remaining, 3000);
            return;
         }
         if(information.DriverLeftVehicle)
         {
            CommonFunctions.DisplayText("Driver left "
                                        + information.VehicleName + "."
                                        + remaining, 3000);
         }
      }

      private bool MadDriversOnRampage()
      {
         if(MadDriversAndTheirVehicles.Count > 0)
            return true;
         CommonFunctions.DisplayText("There are currently no mad drivers on rampage.", 5000);
         return false;
      }

      private void RestrictAmountOfItems()
      {
         if(MadDriversAndTheirVehicles.Count > MAX_AMOUNT_OF_ITEMS)
         {
            MarkItemForNoLongerNeeded(MadDriversAndTheirVehicles[0]);
            MadDriversAndTheirVehicles.RemoveAt(0);
         }
      }
   }
}