﻿using Cyron43.GtaIV.Common.FakesForTests;

namespace Havoc.Tests
{
   internal class FakeCore
   {
      internal FakeCore()
      {
         Configuration = new FakeConfigurationContainer();
      }

      internal FakeConfigurationContainer Configuration { get; set; }

      public bool FakeAffectCops(Ped ped)
      {
         return Configuration.MadCops || ped.PedType != PedType.Cop;
      }
   }

   internal class FakeConfigurationContainer
   {
      internal bool MadCops { get; set; }
   }
}