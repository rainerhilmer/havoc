﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Cyron43.GtaIV.Common;
using FluentAssertions;
using NUnit.Framework;

namespace Havoc.Tests
{
   [TestFixture]
   public class ConfigurationTests
   {
      private ConfigurationContainer _config;
      private ErrorType _typeOfError;

      [TestFixtureSetUp]
      public void Setup()
      {
         _config = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(
               ModIdentityProvider.Identity,
               out _typeOfError,
               supressInGameMessage: true);
      }

      [Test]
      public void Config_is_valid()
      {
         _typeOfError.Should().Be(ErrorType.None);
      }

      [Test]
      public void Read_config_raw_display()
      {
         var sut = _config;
         Console.WriteLine("CarClearers: " + sut.CarClearers);
         Console.WriteLine("CarClearersDriveToWaypoint: " + sut.CarClearersDriveToWaypoint);
         Console.WriteLine("CarClearersObeyTrafficLaws: " + sut.CarClearersObeyTrafficLaws);
         Console.WriteLine("CarClearersPerimeter: " + sut.CarClearersPerimeter);
         Console.WriteLine("CarClearersSpeed: " + sut.CarClearersSpeed);
         Console.WriteLine("CreateCarClearersOutside: " + sut.CreateCarClearersOutside);
         Console.WriteLine("InvolvePlayer: " + sut.InvolvePlayer);
         Console.WriteLine("KeepMadDrivers: " + sut.KeepMadDrivers);
         Console.WriteLine("MadCops: " + sut.MadCops);
         Console.WriteLine("MadDrivers: " + sut.MadDrivers);
         Console.WriteLine("MadDriversObeyTrafficLaws: " + sut.MadDriversObeyTrafficLaws);
         Console.WriteLine("MadDriversSpeed: " + sut.MadDriversSpeed);
         Console.WriteLine("MadPeds: " + sut.MadPeds);
      }

      [Test]
      public void Reads_config()
      {
         var sut = _config;
         sut.CarClearers.Should().BeTrue("CarClearers");
         sut.CarClearersDriveToWaypoint.Should().BeFalse("CarClearersDriveToWaypoint");
         sut.CarClearersObeyTrafficLaws.Should().BeTrue("CarClearersObeyTrafficLaws");
         sut.CarClearersPerimeter.Should().Be(50f, "CarClearersPerimeter");
         sut.CarClearersSpeed.Should().Be(37.28227f, "CarClearersSpeed");
         sut.CreateCarClearersOutside.Should().BeFalse("CreateCarClearersOutside");
         sut.InvolvePlayer.Should().BeFalse("InvolvePlayer");
         sut.KeepMadDrivers.Should().BeFalse("KeepMadDrivers");

         var releaseMadDriversKey = sut.Keys.Find(x => x.Name == "StartHavoc");
         releaseMadDriversKey.Should().NotBeNull("releaseMadDriversKey");
         releaseMadDriversKey.Alt.Should().BeFalse("KeyContainer.Alt");
         releaseMadDriversKey.Ctrl.Should().BeFalse("KeyContainer.Ctrl");
         releaseMadDriversKey.Key.Should().Be(Keys.M, "KeyContainer.Key");
         releaseMadDriversKey.Name.Should().Be("StartHavoc", "KeyContainer.Name");
         releaseMadDriversKey.Shift.Should().BeFalse("KeyContainer.Shift");

         var startHavocKey = sut.Keys.Find(x => x.Name == "StartHavoc");
         startHavocKey.Should().NotBeNull("startHavocKey");
         startHavocKey.Alt.Should().BeFalse("KeyContainer.Alt");
         startHavocKey.Ctrl.Should().BeFalse("KeyContainer.Ctrl");
         startHavocKey.Key.Should().Be(Keys.M, "KeyContainer.Key");
         startHavocKey.Name.Should().Be("StartHavoc", "KeyContainer.Name");
         startHavocKey.Shift.Should().BeFalse("KeyContainer.Shift");

         var toggleBlipMapKey = sut.Keys.Find(x => x.Name == "ToggleBlipMap");
         toggleBlipMapKey.Should().NotBeNull("toggleBlipMapKey");
         toggleBlipMapKey.Alt.Should().BeFalse("KeyContainer.Alt");
         toggleBlipMapKey.Ctrl.Should().BeFalse("KeyContainer.Ctrl");
         toggleBlipMapKey.Key.Should().Be(Keys.M, "KeyContainer.Key");
         toggleBlipMapKey.Name.Should().Be("ToggleBlipMap", "KeyContainer.Name");
         toggleBlipMapKey.Shift.Should().BeTrue("KeyContainer.Shift");

         var triggerCarClearersKey = sut.Keys.Find(x => x.Name == "TriggerCarClearers");
         triggerCarClearersKey.Should().NotBeNull("triggerCarClearersKey");
         triggerCarClearersKey.Alt.Should().BeFalse("KeyContainer.Alt");
         triggerCarClearersKey.Ctrl.Should().BeTrue("KeyContainer.Ctrl");
         triggerCarClearersKey.Key.Should().Be(Keys.C, "KeyContainer.Key");
         triggerCarClearersKey.Name.Should().Be("TriggerCarClearers", "KeyContainer.Name");
         triggerCarClearersKey.Shift.Should().BeFalse("KeyContainer.Shift");

         sut.MadCops.Should().BeFalse("MadCops");
         sut.MadDrivers.Should().BeTrue("MadDrivers");
         sut.MadDriversObeyTrafficLaws.Should().BeTrue("MadDriversObeyTrafficLaws");
         sut.MadDriversSpeed.Should().Be(37.28227f, "MadDriversSpeed");
         sut.MadPeds.Should().BeFalse("MadPeds");
      }

      [Test, Explicit]
      public void Saves_config()
      {
         var keys = new List<KeyContainer>
                    {
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.R,
                          Name = "ReleaseMadDrivers",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.M,
                          Name = "StartHavoc",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.M,
                          Name = "ToggleBlipMap",
                          Shift = true
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = true,
                          Key = Keys.C,
                          Name = "TriggerCarClearers",
                          Shift = false
                       }
                    };
         var config = new ConfigurationContainer
                      {
                         CarClearers = true,
                         CarClearersDriveToWaypoint = false,
                         CarClearersObeyTrafficLaws = true,
                         CarClearersPerimeter = 50f,
                         CarClearersSpeed = 37.28227f,
                         CreateCarClearersOutside = false,
                         InvolvePlayer = false,
                         KeepMadDrivers = false,
                         Keys = keys,
                         MadCops = false,
                         MadDrivers = true,
                         MadDriversObeyTrafficLaws = true,
                         MadPeds = false,
                         MadDriversSpeed = 37.28227f,
                         Version = ModIdentityProvider.Identity.Version
                      };
         ConfigurationProvider.SaveConfig(config, @"a:\HavocConfig.xml");
      }
   }
}