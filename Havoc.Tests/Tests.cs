﻿using Cyron43.GtaIV.Common.FakesForTests;
using FluentAssertions;
using NUnit.Framework;

namespace Havoc.Tests
{
   [TestFixture]
   public class Tests
   {
      [Test]
      public void AffectCops_with_cop_and_mad_cops_true_returns_true()
      {
         var sut = new FakeCore
                   {
                      Configuration = {MadCops = true}
                   };
         sut.FakeAffectCops(new Ped {PedType = PedType.Cop})
            .Should().BeTrue();
      }

      [Test]
      public void AffectCops_with_no_cop_and_mad_cops_true_returns_true()
      {
         var sut = new FakeCore
                   {
                      Configuration = {MadCops = true}
                   };
         sut.FakeAffectCops(new Ped {PedType = PedType.CivFemale})
            .Should().BeTrue();
      }

      [Test]
      public void AffectCops_with_cop_but_mad_cops_false_returns_false()
      {
         var sut = new FakeCore
                   {
                      Configuration = {MadCops = false}
                   };
         sut.FakeAffectCops(new Ped {PedType = PedType.Cop})
            .Should().BeFalse();
      }

      [Test]
      public void AffectCops_with_no_cop_and_mad_cops_false_returns_true()
      {
         var sut = new FakeCore
                   {
                      Configuration = {MadCops = false}
                   };
         sut.FakeAffectCops(new Ped {PedType = PedType.CivFemale})
            .Should().BeTrue();
      }
   }
}